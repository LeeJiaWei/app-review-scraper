import csv
import datetime
import codecs
import numpy as np
from api_functions.get_playstore_rating import get_playstore_rating
import os
import pandas as pd

ratingCountSG = [0] * 6
ratingCountTW = [0] * 6
ratingCountAU = [0] * 6

day = datetime.datetime.now().day
month = datetime.datetime.now().month
year = datetime.datetime.now().year
week = datetime.date(year, month, day).isocalendar()[1] - 1


def isThisWeek(dateInput):
    dateInput = dateInput.partition('T')[0]
    inputYear = int(dateInput.split('-')[0])
    inputMonth = int(dateInput.split('-')[1])
    inputDay = int(dateInput.split('-')[2])
    inputWeek = datetime.date(inputYear, inputMonth, inputDay).isocalendar()[1]
    if inputWeek == week:
        return True
    else:
        return False


with open('docs/googleplay_report_sg.csv', 'r') as googleplay_csv:
    csv_reader = csv.reader(codecs.open('docs/googleplay_report_sg.csv', 'rU', 'utf-16'), delimiter=',')
    data = []
    row_index = 0
    column_index = 0
    next(csv_reader)
    for row in csv_reader:
        if row and isThisWeek(row[5]):
            columns = []
            ratingCountSG[int(row[9])] += 1
    print(np.matrix(ratingCountSG))

    grandTotalSG = sum(ratingCountSG)
    if grandTotalSG == 0:
        avgRatingSG = 0
    else:
        avgRatingSG = (ratingCountSG[1] + ratingCountSG[2] * 2 + ratingCountSG[3] * 3 + ratingCountSG[4] * 4 +
                       ratingCountSG[
                           5] * 5) / grandTotalSG
    storeRatingSG = get_playstore_rating("sg")

with open('docs/googleplay_report_tw.csv', 'r') as googleplay_csv:
    csv_reader = csv.reader(codecs.open('docs/googleplay_report_tw.csv', 'rU', 'utf-16'), delimiter=',')
    data = []
    row_index = 0
    column_index = 0
    next(csv_reader)
    for row in csv_reader:
        if row and isThisWeek(row[5]):
            columns = []
            ratingCountTW[int(row[9])] += 1
    print(np.matrix(ratingCountTW))

    grandTotalTW = sum(ratingCountTW)
    if grandTotalTW == 0:
        avgRatingTW = 0
    else:
        avgRatingTW = (ratingCountTW[1] + ratingCountTW[2] * 2 + ratingCountTW[3] * 3 + ratingCountTW[4] * 4 +
                       ratingCountTW[
                           5] * 5) / grandTotalTW
    storeRatingTW = get_playstore_rating("tw")

with open('docs/googleplay_report_au.csv', 'r') as googleplay_csv:
    csv_reader = csv.reader(codecs.open('docs/googleplay_report_au.csv', 'rU', 'utf-16'), delimiter=',')
    data = []
    row_index = 0
    column_index = 0
    next(csv_reader)
    for row in csv_reader:
        if row and isThisWeek(row[5]):
            columns = []
            ratingCountAU[int(row[9])] += 1
    print(np.matrix(ratingCountAU))

    grandTotalAU = sum(ratingCountAU)
    if grandTotalAU == 0:
        avgRatingAU = 0
    else:
        avgRatingAU = (ratingCountAU[1] + ratingCountAU[2] * 2 + ratingCountAU[3] * 3 + ratingCountAU[4] * 4 +
                       ratingCountAU[
                           5] * 5) / grandTotalAU
    storeRatingAU = get_playstore_rating("au")

with open('docs/android_ratings.csv', 'r') as android_csv:
    csv_reader = csv.reader(android_csv, delimiter=',')
    data = []
    column_index = 0
    for row in csv_reader:
        if row:  # avoid blank lines
            columns = []
            for column_index in range(33):
                columns.append(row[column_index])
            data.append(columns)
    last_row = data[-1]

    # update week number
    last_row[1] = str(week + 1)
    last_row[12] = str(week + 1)
    last_row[23] = str(week + 1)

    # update month number
    last_row[0] = str(month)
    last_row[11] = str(month)
    last_row[22] = str(month)

    # update ratings SG
    last_row[3] = str(ratingCountSG[5])
    last_row[4] = str(ratingCountSG[4])
    last_row[5] = str(ratingCountSG[3])
    last_row[6] = str(ratingCountSG[2])
    last_row[7] = str(ratingCountSG[1])
    last_row[8] = str(grandTotalSG)
    last_row[9] = str(round(avgRatingSG, 3))
    last_row[10] = str(storeRatingSG)

    # update ratings TW
    last_row[14] = str(ratingCountTW[5])
    last_row[15] = str(ratingCountTW[4])
    last_row[16] = str(ratingCountTW[3])
    last_row[17] = str(ratingCountTW[2])
    last_row[18] = str(ratingCountTW[1])
    last_row[19] = str(grandTotalTW)
    last_row[20] = str(round(avgRatingTW, 3))
    last_row[21] = str(storeRatingTW)

    # update ratings AU
    last_row[25] = str(ratingCountAU[5])
    last_row[26] = str(ratingCountAU[4])
    last_row[27] = str(ratingCountAU[3])
    last_row[28] = str(ratingCountAU[2])
    last_row[29] = str(ratingCountAU[1])
    last_row[30] = str(grandTotalAU)
    last_row[31] = str(round(avgRatingAU, 3))
    last_row[32] = str(storeRatingAU)

with open('docs/android_ratings.csv', 'a') as android_csv:
    csv_writer = csv.writer(android_csv)
    csv_writer.writerow(last_row)

df = pd.read_csv('docs/android_ratings.csv')
df.dropna(inplace=True)
df.to_csv('docs/android_ratings.csv', index=False)

os.replace('docs/android_ratings.csv', "docs/output/android_ratings.csv")
