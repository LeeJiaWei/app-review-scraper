import os
import csv
import openpyxl as xl

from api_functions.get_api_services import create_upload_services
from googleapiclient.http import MediaFileUpload

# combine android & ios rating sheets
files = ['docs/output/' + f for f in os.listdir('docs/output')]
wb = xl.Workbook()
wb.remove_sheet(wb.active)

i = 0
for ff in files:
    with open(ff) as f:
        i += 1
        if i == 1:
            ws = wb.create_sheet(f'android_ratings')
        else:
            ws = wb.create_sheet(f'ios_ratings')
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            ws.append(row)

wb.save('circles_app_ratings')

# upload to Google Drive
service = create_upload_services()
folderID = '1CR3T2ZWZZPiRxMAP9zOgi1KIWRpvORQC'
fileID = '1OhVZtR3ZEW_spofPRqe-Dh_iZSRoF_l1Zc56tmcZRUA'
file_name = 'circles_app_ratings'
mime_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'

# for file_name, mime_type in zip(file_names, mime_types):
#     file_metadata = {
#         'name': file_name,
#         'parents': [folderID]
#     }
#
#     media = MediaFileUpload('{0}'.format(file_name), mimetype=mime_type)
#
#     service.files().update(
#         fileId=fileID,
#         body=file_metadata,
#         media_body=media
#     ).execute()

# First retrieve the file from the API.
file = service.files().get(fileId=fileID).execute()

# File's new content.
media_body = MediaFileUpload(
    file_name, mimetype=mime_type, resumable=True)

# Send the request to the API.
service.files().update(
    fileId=fileID,
    media_body=media_body).execute()

