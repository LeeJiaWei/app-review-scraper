import requests
from bs4 import BeautifulSoup


def get_appstore_rating(arg):
    url = 'https://apps.apple.com/sg/app/circles-life-telco-lifestyle/id1056007095'
    consoleURL = 'https://appstoreconnect.apple.com/apps/1056007095/appstore/activity/ios/ratingsResponses?m='
    if arg == "tw":
        url = 'https://apps.apple.com/tw/app/circles-life-%E7%84%A1%E6%A1%86%E8%A1%8C%E5%8B%95/id1459492983'
    if arg == "au":
        url = 'https://apps.apple.com/au/app/circles-life-au/id1473219077'
    r = requests.get(url)

    rating = [0] * 7

    html = r.content
    soup = BeautifulSoup(html, "html.parser")

    rating[0] = soup.find_all('span', {'class': 'we-customer-ratings__averages__display'})[0].text
    # rating[1] =
    # rating[2] =
    # rating[3] =
    # rating[4] =
    # rating[5] =
    rating[6] = soup.find_all('span', {'class': 'we-customer-ratings__averages__display'})[0].text

    return rating
