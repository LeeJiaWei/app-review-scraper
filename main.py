import datetime
import os

spreadsheet_name = "circles_app_ratings"
android_sheet = "android_ratings"
ios_sheet = "ios_ratings"

android_sheet_csv = android_sheet + ".csv"
ios_sheet_csv = ios_sheet + ".csv"

year = str(datetime.datetime.now().year)
month = str(datetime.datetime.now().month)
if int(month) < 10:
    month = "0" + month

googleplay_report_sg_csv = "reviews_com.circles.selfcare_" + year + month + ".csv"
googleplay_report_tw_csv = "reviews_com.circles.selfcare.tw_" + year + month + ".csv"
googleplay_report_au_csv = "reviews_com.circles.selfcare.au_" + year + month + ".csv"

# download all relevant docs
os.system("download_csv.py --spreadsheet-name \"" + spreadsheet_name + "\" --sheet-name " + android_sheet)
os.system("gsutil cp -r gs://pubsite_prod_rev_07758452076776422459/reviews/reviews_com.circles.selfcare_\""
          + year + month + "\".csv .")
os.system("gsutil cp -r gs://pubsite_prod_rev_07758452076776422459/reviews/reviews_com.circles.selfcare.tw_\""
          + year + month + "\".csv .")
os.system("gsutil cp -r gs://pubsite_prod_rev_07758452076776422459/reviews/reviews_com.circles.selfcare.au_\""
          + year + month + "\".csv .")

os.system("download_csv.py --spreadsheet-name \"" + spreadsheet_name + "\" --sheet-name " + ios_sheet)
os.system("download_csv.py --spreadsheet-name \"2020 iOS AppRating\" --sheet-name SGReviews")
os.system("download_csv.py --spreadsheet-name \"2020 iOS AppRating\" --sheet-name TWReviews")
os.system("download_csv.py --spreadsheet-name \"2020 iOS AppRating\" --sheet-name AUReviews")

os.replace(android_sheet_csv, "docs/" + android_sheet_csv)
os.replace(googleplay_report_sg_csv, "docs/googleplay_report_sg.csv")
os.replace(googleplay_report_tw_csv, "docs/googleplay_report_tw.csv")
os.replace(googleplay_report_au_csv, "docs/googleplay_report_au.csv")

os.replace(ios_sheet_csv, "docs/" + ios_sheet_csv)
os.replace("SGReviews.csv", "docs/appstore_report_sg.csv")
os.replace("TWReviews.csv", "docs/appstore_report_tw.csv")
os.replace("AUReviews.csv", "docs/appstore_report_au.csv")

# read google play report & update android sheet
os.system("update_android_sheet.py")
os.system("update_ios_sheet.py")

# upload sheet
os.system("upload_sheet.py")

