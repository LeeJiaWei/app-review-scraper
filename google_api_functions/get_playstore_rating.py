import requests
from bs4 import BeautifulSoup


def get_playstore_rating(arg):
    url = 'https://play.google.com/store/apps/details?id=com.circles.selfcare'
    if arg == "tw":
        url = 'https://play.google.com/store/apps/details?id=com.circles.selfcare.tw'
    if arg == "au":
        url = 'https://play.google.com/store/apps/details?id=com.circles.selfcare.au'
    r = requests.get(url)

    html = r.content
    soup = BeautifulSoup(html, "html.parser")

    rating = soup.select("div.BHMmbe")[0].text

    return rating
