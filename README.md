# AppReviewV2

Downloads Circles.Life app rating counts weekly and updates on Google Sheet.
Apple AppStore capability to be added.

# Setup

Google Cloud API

* Set up credentials for Google Drive API - [here](https://medium.com/swlh/google-drive-api-with-python-part-i-set-up-credentials-1f729cb0372b?source=friends_link&sk=53afa8687344dced6d66c9215ed78840)

* Search for file in Google Drive - [here](https://levelup.gitconnected.com/google-drive-api-with-python-part-ii-connect-to-google-drive-and-search-for-file-7138422e0563?source=friends_link&sk=86c054a6d42c1998f60508f02dae4298)

* Download a Google Sheet as csv - [here](https://medium.com/@billydharmawan/how-to-download-a-specific-sheet-by-name-from-a-google-spreadsheet-as-a-csv-file-e8c7b4b79f39?sk=1c5b217a4eba8991928391da730b6284)

* Install GSutil - [here](https://cloud.google.com/storage/docs/gsutil_install#windows)

Libraries

* datetime

* os

* argparse

* codecs

* numpy

* pandas

* csv

* openpyxl

* googleapiclient

* httplib2

* oauth2client

* pickle

* google_auth_oauthlib.flow

* google.auth.transport.requests

* requests

Changes to repo

* Replace the client_secret.JSON and credentials.JSON inside the "credentials" folder with your own according to the Google Cloud API tutorials

* Download folder https://drive.google.com/drive/folders/1CR3T2ZWZZPiRxMAP9zOgi1KIWRpvORQC?usp=sharing keeping the file names unchanged

* Change folderID and fileID in upload_sheet.PY lines 29 and 30 to your own folderID and fileID

# How to use
Run main.PY
